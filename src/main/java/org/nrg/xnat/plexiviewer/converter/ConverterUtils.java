/*
 * org.nrg.xnat.plexiviewer.converter.ConverterUtils
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.converter;

import org.nrg.xnat.plexiviewer.lite.UserSelection;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;
import org.nrg.xnat.plexiviewer.lite.utils.CreateUtils;

import java.lang.reflect.Constructor;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConverterUtils {
    public synchronized static PlexiImageFile convert(UserSelection u) {
        int    exitStatus         = 0;
        String converterClassName = "org.nrg.plexiviewer.converter.DefaultConverter";
        PlexiImageFile pf = null;
        try {
            Class[] intArgsClass = new Class[] {u.getClass()};
            Object[] intArgs = new Object[] {(UserSelection) u.clone()};
            Constructor intArgsConstructor;
            Class       imgConverterClass = Class.forName(converterClassName);
            intArgsConstructor = imgConverterClass.getConstructor(intArgsClass);
            PlexiLoResConverterI imgConverter = (PlexiLoResConverterI) CreateUtils.createObject(intArgsConstructor, intArgs);
            System.out.println("Invoking " + imgConverter.getClass().getName());
            exitStatus = imgConverter.convertAndSave(u);
            if (exitStatus != 0) {
                System.out.println("Couldnt launch the conveter");
                return pf;
            } else {
                pf = imgConverter.getFileLocationAndName();
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        } catch (NoSuchMethodException e1) {
            System.out.println(e1);
        } catch (URISyntaxException e2) {
            System.out.println(e2);
        } catch (Exception e3) {
            System.out.println(e3);
        }

        System.out.println("Converter Utils created file in " + pf.toString());
        return pf;
    }
}
