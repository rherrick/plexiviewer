/*
 * org.nrg.xnat.plexiviewer.utils.PlexiPublisher
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.utils;

import java.util.Observable;

public class PlexiPublisher extends Observable {
    private Object msg;

    public PlexiPublisher() {
        super();
    }

    public void setValue(Object message) {
        this.msg = message;
        setChanged();
        notifyObservers();
    }

    public Object getValue() {
        return msg;
    }

}
