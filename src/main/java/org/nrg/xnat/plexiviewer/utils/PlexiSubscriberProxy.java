/*
 * org.nrg.xnat.plexiviewer.utils.PlexiSubscriberProxy
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.utils;

import org.nrg.xnat.plexiviewer.manager.PlexiStatusPublisherManager;

import java.util.Observable;
import java.util.Observer;

public class PlexiSubscriberProxy implements Observer {
    boolean isRegistered = false;
    String options;
    boolean hasUpdate = false;
    Object message;

    public PlexiSubscriberProxy(String opt) {
        options = opt;
        register();
    }

    public void update(Observable publisher, Object args) {
        message = ((PlexiPublisher) publisher).getValue();
        hasUpdate = true;
    }

    public void register() {
        if (!isRegistered) {
            PlexiPublisher publisher = PlexiStatusPublisherManager.GetInstance().getPublisher(options);
            if (publisher != null) {
                publisher.addObserver(this);
                isRegistered = true;
            }
        }
    }

    public boolean hasUpdate() {
        return hasUpdate;
    }

    public Object getMessage() {
        return message;
    }
}
