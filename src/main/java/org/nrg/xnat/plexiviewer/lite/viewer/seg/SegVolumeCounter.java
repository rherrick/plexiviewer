/*
 * org.nrg.xnat.plexiviewer.lite.viewer.seg.SegVolumeCounter
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plexiviewer.lite.viewer.seg;

import org.nrg.xnat.plexiviewer.lite.utils.HTTPDetails;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SegVolumeCounter implements Runnable {

    int[]   volCounts;
    String  sessionId;
    boolean isDone;
    private String host;

    public SegVolumeCounter(String sessionId, String host) {
        this.sessionId = sessionId;
        this.host = host;
        isDone = false;
        volCounts = new int[140];
    }

    public void setVolumeCounts() {
        Thread vCounter = new Thread(this);
        vCounter.start();
    }

    public void run() {
        getCounts();
        isDone = true;
    }

    private void getCounts() {
        String        suffix          = HTTPDetails.getSuffix("GetAsegRegionVolumes");
        URL           populateServlet = null;
        URLConnection servletConnection;
        try {
            suffix += "?sessionId=" + sessionId;
            populateServlet = HTTPDetails.getURL(host, suffix);
            servletConnection = HTTPDetails.openConnection(populateServlet);
            //Don't use a cached version of URL connection.
            servletConnection.setUseCaches(false);
            InputStream       is                     = servletConnection.getInputStream();
            ObjectInputStream inputStreamFromServlet = new ObjectInputStream(is);
            for (int i = 0; i < 140; i++) {
                volCounts[i] = inputStreamFromServlet.readInt();
            }
            inputStreamFromServlet.close();
            is.close();
        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    public boolean isDone() {
        return isDone;
    }

    public int[] getVolumeCount() {
        return volCounts;
    }


}
