/*
 * org.nrg.xnat.plexiviewer.lite.display.PlexiCoordinates
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.display;

import org.nrg.xnat.plexiviewer.lite.xml.Layout;

public class PlexiCoordinates {

    MontageDisplay mDisplay;
    int            radiologic;
    String         view;
    Point3d        bBoxStart, bboxDim, imageDim; //Bounding box and its dimensions
    Point3d origin;
    int     scale;
    int     voxelSize;
    int     montageIncrement;
    int     nMontageColumns;
    Point3d talPos;
    float   montageScale;
    boolean nativeSpace;

    public PlexiCoordinates(int imgX, int imgY, int imgZ, MontageDisplay mDisplay, String view, boolean radiologic, int nColumns) {
        bBoxStart = new Point3d();
        Layout l = mDisplay.getLayout();
        this.mDisplay = mDisplay;
        if (l.getName().equalsIgnoreCase("TAL_111")) {
            bboxDim = new Point3d(176, 208, 176);
        } else if (l.getName().equalsIgnoreCase("TAL_222")) {
            bboxDim = new Point3d(128, 128, 75);
        } else if (l.getName().equalsIgnoreCase("TAL_333")) {
            bboxDim = new Point3d(48, 64, 48);
        } else {
            bboxDim = new Point3d(imgX, imgY, imgZ);
        }
        imageDim = new Point3d(imgX, imgY, imgZ);
        origin = l.getOrigin();
        scale = 1;
        montageIncrement = mDisplay.getSliceSpacing();
        montageScale = mDisplay.getScale();
        voxelSize = l.getVoxelSize();
        this.radiologic = radiologic ? -1 : 1;
        this.view = view;
        nMontageColumns = nColumns;
        talPos = new Point3d();
        nativeSpace = l.getName().equalsIgnoreCase("native");
    }

    public void setTalFromStack(int winX, int winY, int slice) {
        winX = winX / scale;
        winY = winY / scale;

        if (nativeSpace) {
            talPos.setX(winX);
            talPos.setY(winY);
            talPos.setZ(slice);
        } else {
            //Note: the calculation for z in the coronal and sagittal views is a bit strange b/c
            //the origin is specified from the _bottom_ of the box, while winY starts from the _top_ of the box
            if (view.equalsIgnoreCase("TRANSVERSE")) {
                talPos.setX((radiologic * (winX - origin.getX()) * voxelSize));
                talPos.setY(((origin.getY() - winY) * voxelSize));
                talPos.setZ(((slice - origin.getZ()) * voxelSize));
            } else if (view.equalsIgnoreCase("CORONAL")) {
                talPos.setX((radiologic * (winX - origin.getX()) * voxelSize));
                talPos.setY((((bboxDim.getZ() - origin.getZ()) - winY) * voxelSize));
                talPos.setZ(((origin.getY() - slice) * voxelSize));
            } else if (view.equalsIgnoreCase("SAGITTAL")) {
                talPos.setX(((origin.getY() - winX) * voxelSize));
                talPos.setY((((bboxDim.getZ() - origin.getZ()) - winY) * voxelSize));
                talPos.setZ(((slice - origin.getX()) * voxelSize));
            }
        }
    }

    public void setTalFromMontage(int winX, int winY) {
        int x, y, z, sliceNum;

        winX = winX / scale;
        winY = winY / scale;
        int montageStartSlice = mDisplay.getStartSlice();
        if (nativeSpace) {
            int   mSliceNum, row, col;
            float rowHeight, colWidth;
            rowHeight = (montageScale * bboxDim.getY());
            colWidth = (montageScale * bboxDim.getX());
            row = (int) Math.floor(winY / rowHeight);
            col = (int) Math.ceil(winX / colWidth);
            mSliceNum = montageStartSlice + (row * nMontageColumns + col - 1) * montageIncrement;
            //System.out.println("WinX " + winX + "\t " + winY);
            //System.out.println("Slice Num " + SliceNum + "\t Row " + row + "\t Col " + col);
            //System.out.println("Row  Height " + rowHeight + "\t Col Width " + colWidth);
            talPos.setZ(mSliceNum);
            talPos.setX(winX - (col - 1) * colWidth);
            talPos.setY(winY - row * rowHeight);
            talPos.setX(talPos.getX() / montageScale);
            talPos.setY(talPos.getY() / montageScale);
            //System.out.println("TalX " + talPos.getX() + "\t Y " + talPos.getY() + "\t Z " + talPos.getZ());
        } else {
            //Note: the calculation for z in the coronal and sagittal views is a bit strange b/c the origin is specified from the
            //_bottom_ of the box, while winY starts from the _top_ of the box

            if (view.equalsIgnoreCase("TRANSVERSE")) {
                sliceNum = ((int) Math.floor(winY / (montageScale * bboxDim.getY())) * nMontageColumns) + ((int) Math.ceil(winX / (montageScale * bboxDim.getX())));
                int bboxDimXY = (int) (bboxDim.getX() * montageScale);
                talPos.setZ(((montageStartSlice + (montageIncrement * (sliceNum - 1)) - origin.getZ()) * voxelSize));
                x = (winX) % bboxDimXY;                            //remove offset from previous slices in row
                talPos.setX((radiologic * ((int) (x / montageScale) - origin.getX()) * voxelSize));
                bboxDimXY = (int) (bboxDim.getY() * montageScale);
                y = (winY % bboxDimXY);
                talPos.setY(((origin.getY() - (int) (y / montageScale)) * voxelSize));
            } else if (view.equalsIgnoreCase("CORONAL")) {
                sliceNum = ((int) Math.floor(winY / (montageScale * bboxDim.getZ())) * nMontageColumns) + ((int) Math.ceil(winX / (montageScale * bboxDim.getX())));
                talPos.setZ(((origin.getY() - (montageStartSlice + (montageIncrement * (sliceNum - 1)))) * voxelSize));
                int bboxDimXY = (int) (bboxDim.getX() * montageScale);
                x = winX % bboxDimXY;
                talPos.setX((radiologic * ((int) (x / montageScale) - origin.getX()) * voxelSize));
                bboxDimXY = (int) (bboxDim.getZ() * montageScale);
                z = winY % bboxDimXY;
                talPos.setY((-((int) (z / montageScale) + (origin.getZ() - bboxDim.getZ())) * voxelSize));

            } else if (view.equalsIgnoreCase("SAGITTAL")) {
                sliceNum = ((int) Math.floor(winY / (montageScale * bboxDim.getZ())) * nMontageColumns) + ((int) Math.ceil(winX / (bboxDim.getY() * montageScale)));
                talPos.setZ((((montageStartSlice + (montageIncrement * (sliceNum - 1))) - origin.getX()) * voxelSize));
                int bboxDimXY = (int) (bboxDim.getY() * montageScale);
                y = winX % bboxDimXY;
                talPos.setX(((origin.getY() - y / montageScale) * voxelSize));
                bboxDimXY = (int) (bboxDim.getZ() * montageScale);
                z = winY % bboxDimXY;
                talPos.setY(((bboxDim.getZ() - origin.getZ()) - (int) (z / montageScale)) * voxelSize);
            }
        }
        //System.out.println("WinX " + winX + " winY " + winY + "TAL " + talPos.toString());
    }

    public void setTalFromSlice(int slice) {
        //slice = slice - 1;
        if (nativeSpace) {
            talPos.setZ(slice);
        } else {
            if (view.equalsIgnoreCase("TRANSVERSE")) {
                talPos.setZ(((slice - origin.getZ()) * voxelSize));
            } else if (view.equalsIgnoreCase("CORONAL")) {
                talPos.setZ(((origin.getY() - slice) * voxelSize));
            } else if (view.equalsIgnoreCase("SAGITTAL")) {
                talPos.setZ(((slice - origin.getX()) * voxelSize));
            }
        }
    }


    public Point3d getPosWindow(String display, String fromView, boolean fromRadiologic) {
        int x                 = 0, y = 0, z = 0;
        int montageStartSlice = mDisplay.getStartSlice();

        if (display.equalsIgnoreCase("STACK")) {
            if (nativeSpace) {
                //System.out.println(radiologic + " NATIVE STACK " + rtn.toString());
                return PlexiNativeCoordinates.getNativeCoordinates(talPos, fromView, view, (radiologic == -1), imageDim, fromRadiologic, this.scale);
            } else {
                return PlexiNativeCoordinates.getTalCoordinates(talPos, fromView, view, radiologic, bboxDim, imageDim, origin, voxelSize, this.scale, fromRadiologic);
            }
        } else if (display.equalsIgnoreCase("MONTAGE")) {
            int nBox = 0, nRow, nColumn;
            if (nativeSpace) {
                nBox = (int) ((montageStartSlice - talPos.getZ()) / Math.abs(montageIncrement));
                nRow = nBox / nMontageColumns;
                nColumn = nBox - (nRow * nMontageColumns) - 1;
                System.out.println("Reverse Comp BOX: " + nBox + "\t Row " + nRow + "\t Col " + nColumn);
                x = (int) (nColumn * bboxDim.getX()) + (int) (talPos.getX() / voxelSize);
                y = (int) (nRow * bboxDim.getY()) + (int) (talPos.getY() / voxelSize);
                z = (int) talPos.getZ();
                return PlexiNativeCoordinates.getNativeCoordinates(x, y, z, fromView, view, (radiologic == -1), imageDim, fromRadiologic, this.scale);

            } else {
                if (view.equalsIgnoreCase("TRANSVERSE")) {
                    nBox = (int) ((montageStartSlice - talPos.getZ() / voxelSize - origin.getZ()) / Math.abs(montageIncrement));
                    nRow = nBox / nMontageColumns;
                    nColumn = nBox - (nRow * nMontageColumns);
                    x = (int) ((nColumn * bboxDim.getX()) + (int) (origin.getX() + (talPos.getX() / voxelSize)));
                    y = (int) ((nRow * bboxDim.getY()) + (int) (origin.getY() - (talPos.getY() / voxelSize)));
                } else if (view.equalsIgnoreCase("CORONAL")) {
                    nBox = (int) (-(montageStartSlice + talPos.getZ() / voxelSize - origin.getY()) / Math.abs(montageIncrement));
                    nRow = nBox / nMontageColumns;
                    nColumn = nBox - (nRow * nMontageColumns);
                    x = (int) ((nColumn * bboxDim.getX()) + (int) (origin.getX() + (talPos.getX() / voxelSize)));
                    y = (int) ((nRow * bboxDim.getZ()) - (int) ((talPos.getY() / voxelSize) + origin.getZ() - bboxDim.getZ()));
                } else if (view.equalsIgnoreCase("SAGITTAL")) {
                    nBox = (int) ((talPos.getZ() / voxelSize - montageStartSlice + origin.getX()) / Math.abs(montageIncrement));
                    nRow = nBox / nMontageColumns;
                    nColumn = nBox - (nRow * nMontageColumns);
                    x = (int) ((nColumn * bboxDim.getY()) + (int) (origin.getY() - (talPos.getX() / voxelSize)));
                    y = (int) ((nRow * bboxDim.getZ()) - (int) ((talPos.getY() / voxelSize) + origin.getZ() - bboxDim.getZ()));
                }
            }
            z = nBox;
        }
        //System.out.println("Display is "+ display + " View is " + view + "WinX " + x + " winY " + y + "TAL " + talPos.toString());
        return new Point3d(x * scale, y * scale, z);
    }


    //	return current tailarach coords
    public Point3d getPosTal() {
        return talPos;
    }

    public void setTalFromTal(Point3d p) {
        talPos.set(p);
    }

    public void setTalFromTal(int x, int y, int z) {
        talPos.set(x, y, z);
    }


    /**
     * @param point3d
     */
    public void setBboxDim(Point3d point3d) {
        bboxDim = point3d;
    }


    /**
     * @return
     */
    public int getScale() {
        return scale;
    }

    /**
     * @param i
     */
    public void setScale(int i) {
        scale = i;
    }

}


