/*
 * org.nrg.xnat.plexiviewer.lite.display.PlexiNativeCoordinates
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.display;

public class PlexiNativeCoordinates {


    public static Point3d getNativeCoordinates(Point3d winPos, String fromView, String toView, boolean radiologic, Point3d imgDim, boolean fromRadiologic, int scaleFactor) {
        return getNativeCoordinates((int) winPos.getX(), (int) winPos.getY(), (int) winPos.getZ(), fromView, toView, radiologic, imgDim, fromRadiologic, scaleFactor);
    }

    public static Point3d getTalCoordinates(Point3d talPos, String fromView, String toView, int radiologic, Point3d bboxDim, Point3d imgDim, Point3d origin, int voxelSize, int scaleFactor, boolean fromRadiologic) {
        Point3d rtn = new Point3d();
        int     x   = 0, y = 0, z = 0;
        if (fromView.equalsIgnoreCase("SAGITTAL")) {
            if (toView.equalsIgnoreCase("CORONAL")) {
                x = (int) (radiologic * talPos.getZ() / voxelSize + origin.getX());
                y = (int) ((bboxDim.getZ() - origin.getZ()) - (talPos.getY() / voxelSize));
                z = (int) imgDim.getZ() - (int) origin.getY() + (int) (talPos.getX() / voxelSize);
            } else if (toView.equalsIgnoreCase("TRANSVERSE")) {
                x = (int) (radiologic * talPos.getZ() / voxelSize + origin.getX());
                y = (int) (origin.getY() - talPos.getX() / voxelSize);
                z = (int) (imgDim.getZ() - (int) ((bboxDim.getZ() - origin.getZ()) - (talPos.getY() / voxelSize)));
            } else if (toView.equalsIgnoreCase("SAGITTAL")) {
                x = (int) (origin.getY() - talPos.getX() / voxelSize);
                y = (int) ((bboxDim.getZ() - origin.getZ()) - (talPos.getY() / voxelSize));
                z = (int) origin.getX() + (int) (talPos.getZ() / voxelSize);
            }
        } else if (fromView.equalsIgnoreCase("TRANSVERSE")) {
            if (toView.equalsIgnoreCase("TRANSVERSE")) {
                x = (int) (radiologic * (talPos.getX() / voxelSize) + origin.getX());
                y = (int) (origin.getY() - talPos.getY() / voxelSize);
                z = (int) (talPos.getZ() / voxelSize) + (int) origin.getZ();
            } else if (toView.equalsIgnoreCase("CORONAL")) {
                x = (int) (radiologic * talPos.getX() / voxelSize + origin.getX());
                y = (int) ((bboxDim.getZ() - origin.getZ()) - (talPos.getZ() / voxelSize));
                z = (int) imgDim.getZ() - (int) origin.getY() + (int) (talPos.getY() / voxelSize);
            } else if (toView.equalsIgnoreCase("SAGITTAL")) {
                x = (int) (origin.getY() - talPos.getY() / voxelSize);
                y = (int) ((bboxDim.getZ() - origin.getZ()) - (talPos.getZ() / voxelSize));
                z = (int) origin.getX() + (int) (talPos.getX() / voxelSize);
            }
        } else if (fromView.equalsIgnoreCase("CORONAL")) {
            if (toView.equalsIgnoreCase("CORONAL")) {
                x = (int) (radiologic * talPos.getX() / voxelSize + origin.getX());
                y = (int) ((bboxDim.getZ() - origin.getZ()) - (talPos.getY() / voxelSize));
                z = (int) origin.getY() - (int) (talPos.getZ() / voxelSize);
            } else if (toView.equalsIgnoreCase("SAGITTAL")) {
                x = (int) imgDim.getX() - (int) (origin.getY() - talPos.getZ() / voxelSize);
                y = (int) ((bboxDim.getZ() - origin.getZ()) - (talPos.getY() / voxelSize));
                z = (int) origin.getX() + (int) (talPos.getX() / voxelSize);
            } else if (toView.equalsIgnoreCase("TRANSVERSE")) {
                x = (int) (radiologic * (talPos.getX() / voxelSize) + origin.getX());
                y = (int) imgDim.getY() - (int) (origin.getY() - talPos.getZ() / voxelSize);
                z = (int) (talPos.getY() / voxelSize) + (int) origin.getZ();
            }
        }

        rtn.set(x * scaleFactor, y * scaleFactor, z);
        //System.out.println(toView + " WIN COORDINATES " + rtn.toString());
        return rtn;
    }

    public static Point3d getNativeCoordinates(int x, int y, int z, String fromView, String toView, boolean radiologic, Point3d imgDim, boolean fromRadiologic, int scaleFactor) {
        Point3d rtn = new Point3d(x, y, z);
        if (fromView.equalsIgnoreCase("SAGITTAL")) {
            if (toView.equalsIgnoreCase("CORONAL")) {
                rtn = new Point3d(z, y, imgDim.getZ() - x);
                if (radiologic) {
                    Point3d old = rtn;
                    rtn.setX(imgDim.getX() - old.getX()); //img Width - old X as a flip is done for radiologic view
                }
            } else if (toView.equalsIgnoreCase("TRANSVERSE")) {
                rtn = new Point3d(z, x, imgDim.getZ() - y);
                if (radiologic) {
                    Point3d old = rtn;
                    rtn.setX(imgDim.getX() - old.getX()); //img Width - old X as a flip is done for radiologic view
                }
            }//else if (toView.equalsIgnoreCase("SAGITTAL")) {
            //  if ((radiologic && !fromRadiologic) || (!radiologic && fromRadiologic))
            //     rtn = new Point3d(imgDim.getX() - x,y,imgDim.getZ()-z);
            //}
        } else if (fromView.equalsIgnoreCase("TRANSVERSE")) {
            if (toView.equalsIgnoreCase("SAGITTAL")) {
                rtn = new Point3d(y, imgDim.getY() - z, x);
                if (fromRadiologic) {
                    Point3d old = rtn;
                    rtn.setZ(imgDim.getZ() - rtn.getZ());
                }
                // if (radiologic) {
                //    rtn.setZ(imgDim.getZ() - rtn.getZ());
                //    rtn.setX(imgDim.getX() - rtn.getX());
                // }
            } else if (toView.equalsIgnoreCase("CORONAL")) {
                rtn = new Point3d(x, imgDim.getY() - z, imgDim.getZ() - y);
                if ((radiologic && !fromRadiologic) || (!radiologic && fromRadiologic)) {
                    Point3d old = rtn;
                    rtn.setX(imgDim.getX() - old.getX()); //img Width - old X as a flip is done for radiologic view
                }
            } else {
                if ((radiologic && !fromRadiologic) || (!radiologic && fromRadiologic)) {
                    Point3d old = rtn;
                    rtn.setX(imgDim.getX() - old.getX()); //img Width - old X as a flip is done for radiologic view
                }
            }
        } else if (fromView.equalsIgnoreCase("CORONAL")) {
            if (toView.equalsIgnoreCase("SAGITTAL")) {
                rtn = new Point3d(imgDim.getX() - z, y, x);
                if (fromRadiologic) {
                    rtn.setZ(imgDim.getZ() - rtn.getZ());
                }
                //if (radiologic) {
                //   rtn.setZ(imgDim.getZ() - rtn.getZ());
                //   rtn.setX(imgDim.getX() - rtn.getX());
                // }
            } else if (toView.equalsIgnoreCase("TRANSVERSE")) {
                rtn = new Point3d(x, imgDim.getY() - z, imgDim.getZ() - y);
                if ((radiologic && !fromRadiologic) || (!radiologic && fromRadiologic)) {
                    rtn.setX(imgDim.getX() - rtn.getX()); //img Width - old X as a flip is done for radiologic view
                }
            } else {
                if ((radiologic && !fromRadiologic) || (!radiologic && fromRadiologic)) {
                    rtn.setX(imgDim.getX() - rtn.getX()); //img Width - old X as a flip is done for radiologic view
                }
            }
        }
        Point3d old = rtn;
        rtn.set(old.getX() * scaleFactor, old.getY() * scaleFactor, old.getZ());
        return rtn;
    }


}
