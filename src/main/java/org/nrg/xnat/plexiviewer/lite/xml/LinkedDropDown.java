/*
 * org.nrg.xnat.plexiviewer.lite.xml.LinkedDropDown
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.lite.xml;

public class LinkedDropDown implements Cloneable {
    String viewableItemType;

    /**
     * @return
     */
    public String getViewableItemType() {
        return viewableItemType;
    }


    public String toString() {
        return viewableItemType;
    }

    /**
     * @param string
     */
    public void setViewableItemType(String string) {
        viewableItemType = string;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
