/*
 * org.nrg.xnat.plexiviewer.lite.ui.GenericIdentifier
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.lite.ui;

import java.io.Serializable;

/**
 * @author Mohana
 */
public class GenericIdentifier implements Serializable {
    private Object id;
    private Object value;
    private boolean sendId = false;

    public GenericIdentifier(Object id, Object value) {
        this.id = id;
        this.value = value;
    }

    public GenericIdentifier() {
    }

    public GenericIdentifier(Object id, Object value, boolean sendid) {
        this.id = id;
        this.value = value;
        this.sendId = sendid;
    }


    /**
     * @return The object ID.
     */
    public Object getId() {
        return id;
    }

    /**
     * @return The value.
     */
    public Object getValue() {
        return value;
    }

    /**
     * @param object    The ID to set.
     */
    public void setId(Object object) {
        id = object;
    }

    /**
     * @param object    The value to set.
     */
    public void setValue(Object object) {
        value = object;
    }

    public String toString() {
        String rtn = value.toString();
        if (sendId) {
            rtn = id.toString();
        }
        return rtn;
    }

}
